const amqp = require('amqplib');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27000/bookstore', {
  useNewUrlParser: true
});

const Book = require('./models/Book');

const QUEUE = 'test';
let _ch;

amqp.connect('amqp://localhost')
.then(connection => {
  return connection.createChannel();
}).then(channel => {
  return channel.assertQueue(QUEUE, { durable: false }).then(ok => {
    return channel.consume(QUEUE, msg => {
      const rawRequestBody = JSON.parse(msg.content.toString());
      const { data } = rawRequestBody;
      return Book.create(data).then(resp => {
        console.log(resp);
      });
    }, { noAck: true });
  });
});