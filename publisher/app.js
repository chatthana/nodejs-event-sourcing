const express = require('express');
const bodyParser = require('body-parser');
const amqp = require('amqplib');

const app = express();

const QUEUE = 'test';

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/send', (req, res) => {

  const dataToBeSent = JSON.stringify(req.body);

  amqp.connect('amqp://localhost')
  .then(connection => {
    return connection.createChannel();
  }).then(channel => {
    return channel.assertQueue(QUEUE, { durable: false }).then(ok => {
      channel.sendToQueue(QUEUE, new Buffer(dataToBeSent));
      return res.json({
        status: '000',
        message: 'Success',
        data: dataToBeSent
      })
    });
  });
});

app.listen(3000, () => {
  console.log('The application is initialised and running on the port %s', 3000);
});

